/* Copyright 2012 Castle Technology Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>

#include "DebugLib/DebugLib.h"
#include "callx/callx.h"
#include "swis.h"
#include "sys/callout.h"
#include "sys/time.h"
#include "Global/HALEntries.h"
#include "AsmUtils/irqs.h"

#include "cmodule.h"

#include "dwc_os.h"

void splx (int s)
{
    restore_irqs(s);
//    dprintf (("", "int on\n"));
}

int splbio (void)
{
    int s = ensure_irqs_off();
//    dprintf (("", "int off\n"));
    return s;
}

/*
 * Callouts
 */
extern void callout_veneer (void);

void
callout_init (struct callout* c)
{
#ifdef USB_DEBUG0
    if (usbdebug > 15) dprintf (("Port", "callout init %p\n", c));
#endif
    memset (c, 0, sizeof (*c));
}

_kernel_oserror*
callout_handler (_kernel_swi_regs* r, void* pw, void* _c) {
    struct callout* c = _c;
#ifdef USB_DEBUG0
    if (usbdebug > 15) dprintf (("Port", "callout2 %p called\n", c));
#endif
c->c_next = NULL;
    if(c->c_func)c->c_func (c->c_arg);
    return NULL;
}

void
callout_stop (struct callout *c) {
#ifdef USB_DEBUG0
    if (usbdebug > 15) dprintf (("Port", "callout stop %p\n", c));
#endif
c->c_next = NULL;
    callx_remove_callafter (callout_handler, c);
}

void
callout_reset (struct callout *c, int i, void (*f)(void *), void *v) {
    if (i <= 0) i = 1;
    dwc_assert(!c->c_next,"");
    c->c_arg = v;
    c->c_func = f;
    c->c_next = (void*) private_word; /* hacky thing to get handler to work */
#ifdef USB_DEBUG0
    if (usbdebug > 15) dprintf (("Port", "callout %p reset %dms\n", c, i));
#endif
    callx_add_callafter ((i + 9) / 10, callout_handler, c);
}

void* vtophys (void** v)
{
#ifndef EMULATE
    struct {
        void*   page;
        void*   logical;
        void*   physical;
    } block;
    block.logical = v;
    _swix (OS_Memory, _INR (0, 2), (1<<9) + (1<<13), &block, 1);

    return block.physical;
#else
//    dprintf (("Port", "Converting physical address %p\n", *v));
    return v; // return actual address for the moment
#endif
}

void* malloc_contig(int len, int alignment)
{
    void* p;
    _kernel_oserror* e;

    e = _swix(PCI_RAMAlloc, _INR(0,2)|_OUT(0), len, alignment, 0, &p);
    if (e || !p)
    {
        dprintf (("", "failed to allocate %d bytes at %d alignment err = '%s'\n",
            len, alignment, e?e->errmess:""));

        return NULL;
    }

    memset(p, 0, len);

    return p;
}

void free_contig (void **mem)
{
    _swix(PCI_RAMFree, _IN(0), *mem);
}

int min (int a, int b)
{
    if (a < b) return a;
    return b;
}

void logprintf (char* format, ...)
{
    va_list p;
    va_start (p, format);
    dvprintf (("Log", format, p));
    va_end (p);
}
